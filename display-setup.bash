#!/bin/bash
#
# File: display-setup.bash
#
# Copyright 2020 Andanan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

declare -g _result
declare -g _xrandr_q
declare -g app_name=$(basename $0)
declare -g app_version='1.0.0'
declare -gA deviceNamesByOutput

# exit on error (fail-fast)
set -e

copy_hash() {
	local oldHash="$1"
	local newHash="$2"
	local __copy__=$(declare -p "$oldHash")
	eval "declare -A __copy__='${__copy__#*=}'"
	for i in "${!__copy__[@]}"; do
		eval "${newHash}['$i']='${__copy__["$i"]}'"
	done
}

xrandr_query() {
	if [[ -z $_xrandr_q ]]; then
		_xrandr_q=$(xrandr -q)
	fi
	echo "$_xrandr_q"
}

get_connected_outputs() {
	unset _result
	declare -ag _result=()
	for output in $(xrandr_query | awk '$2 == "connected" {print $1}' | sort -n); do
		_result+=("$output")
	done
}

get_all_output_names() {
	_result=()
	for output in $(xrandr_query | awk '$2 ~ /^(dis)?connected$/ {print $1}' | sort -n); do
		_result+=("$output")
	done
}

get_device_names_by_output_names() {
	# Source: https://stackoverflow.com/a/24933353
	if [[ ${#deviceNamesByOutput[@]} -eq 0 ]]; then
		while read -r output hex; do
			deviceNamesByOutput["$output"]=$(xxd -r -p <<< "$hex")
		done < <(xrandr --prop |
			awk '
				!/^[ \t]/ {
					if (output && hex) print output, hex
					output=$1
					hex=""
				}
				/[:.]/ && h {
					sub(/.*000000fc00/, "", hex)
					hex = substr(hex, 0, 26) "0a"
					sub(/0a.*/, "", hex)
					h=0
				}
				h {sub(/[ \t]+/, ""); hex = hex $0}
				/EDID.*:/ {h=1}
				END {if (output && hex) print output, hex}
			' | sort
		)
	fi
	unset _result
	declare -Ag _result
	copy_hash deviceNamesByOutput _result
}

print_modes_for_output() {
	xrandr_query | awk -v output="$1" '
		$2 ~ /^(dis)?connected$/ {
			current_output=$1
		}
		current_output == output && $1 != current_output {
			print $1
		}
	' | sort -nr
}

get_highest_resolution() {
	# returns highest supported resolution for a given output
	[[ -z $1 ]] && return 1
	_result=$(print_modes_for_output "$1" | head -n1)
}

get_output_with_highest_resolution() {
	_result=$(
		(
			get_connected_outputs
			for output in "${_result[@]}"; do
				get_highest_resolution "$output"
				echo "$_result $output"
			done
		) | sort -n | tail -n1 | awk '{print $2}'
	)
}

does_output_exist() {
	get_connected_outputs
	for output in "${_result[@]}"; do
		if [[ $output == $1 ]]; then
			return 0
		fi
	done
	return 1
}

get_primary_output() {
	if [[ -n $PRIMARY_DISPLAY ]]; then
		_result="$PRIMARY_DISPLAY"
		return
	fi
	get_output_with_highest_resolution
}

print_usage() {
	# define app_name spacer
	app_nmsp=' '
	while [[ ${#app_nmsp} -lt ${#app_name} ]]; do
		app_nmsp+="$app_nmsp"
	done
	app_nmsp="${app_nmsp:0:${#app_name}}"
	cat <<- EOF
		Usage: $app_name DISPLAY_CONFIG [DISPLAY_CONFIG...]
		       $app_nmsp (-a|--autosetup)
		       $app_nmsp (-h|--help)
		       $app_nmsp (-l|--list-displays) [--detail]
		       $app_nmsp (-L)
		       $app_nmsp (-m|--list-modes) [OUTPUT_NAME...]
		       $app_nmsp (-V|--version)

	EOF
}

print_help() {
	cat <<- EOF
		$(print_usage)

		Options:
		  -a|--autosetup       automatically configure all connected outputs with
		                       their highest possible resolution
		  -h|--help            print this help and exit
		  -l|--list-displays   list all connected (available) displays
		     --detail          show more detailed info about the connected displays
		                       (only valid together with -l or --list-displays)
		  -L                   shorthand for \`$app_name --list-displays --detail\`
		  -m|--list-modes      list all available modes for all, or the specified
		                       outputs
		  -V|--version         print version info and exit

		Format of DISPLAY_CONFIG:
		   <OUTPUT_NAME>:[<MODE_SPEC>][:[ADDITIONAL_ARGS]]
		      OUTPUT_NAME:     the name identifier of the output (list them via the
		                       -l or --list-displays options)
		      MODE_SPEC:       the mode specification or a valid mode name
		      ADDITIONAL_ARGS: additional args to the xrandr command (see below)
		   <OUTPUT_NAME>:-
		      disables the output

		Format of MODE_SPEC:
		   <WIDTH>x<HEIGHT>[@<RATE>]
		      WIDTH:  the screen width in pixels
		      HEIGHT: the screen height in pixels
		      RATE:   the refresh rate in Hz
		   <MODE_NAME>
		      a valid mode name as listed via \`display-setup -m <OUTPUT_NAME>\`

		Format of ADDITIONAL_ARGS:
		   <ADDITIONAL_ARG>[:[<ADDITIONAL_ARGS]]
		      ADDITIONAL_ARG:  an additional arg definition (see below)
		      ADDITIONAL_ARGS: more additional args (arbitrarily chainable,
		                       separated by colons)

		Format of ADDITIONAL_ARG:
		   <OPT>[=<VALUE>]
		      OPT:   A short or long option as defined by the xrandr manpage.
		             (Special addition: the short option `p` evaluates to
		             the long xrandr option `--primary`)
		      VALUE: An (optional) value to the given option. If a value is
		             missing, the <OPT> is treated as simple flag and no value is
		             passed to the xrandr command.
		      (See the xrandr manpage for more info about the available options)

		Examples:

		   $app_name display-setup -a

		      Automatically setup all connected displays to their highest
		      possible screen resolution. The screen with the largest possible
		      screen resolution will be set as primary screen.


		   $app_name HDMI-1::p
  
		      Setup display `HDMI-1` to be the primary display with its highest
		      possible resolution.


		   $app_name HDMI-1:1920x1080

		      Setup display `HDMI-1` for a screen resolution of 1920x1080 px.


		   $app_name HDMI-1:1920x1080@60

		      Setup display `HDMI-1` for a screen resolution of 1920x1080 px at
		      60Hz.


		   $app_name HDMI-1:-

		      Disable display `HDMI-1`.


		   $app_name HDMI-1::p DP-1::right-of=HDMI-1

		      Setup display `HDMI-1` as the primary display with its highest
		      possible resolution and `DP-1` as secondary display with its
		      highest possible resolution. Positions display `DP-1` to the right
		      of display `HDMI-1`.
		      (see the `--right-of` option in the xrandr manpage)

	EOF
}

get_details_for_output() {
	get_device_names_by_output_names
	unset _result
	declare -Ag _result
	_result['device_name']="${deviceNamesByOutput[$1]}"
	local num='[0-9][0-9]*'
	local sizePos=$(xrandr_query | sed -n "s/^${1}.* \(${num}x${num}+${num}+${num}\).*/\1/p")
	local resolution="${sizePos%%+*}"
	local width="${resolution%%x*}"
	local height="${resolution#*x}"
	_result['width']="$width"
	_result['height']="$height"
	local position="${sizePos#*+}"
	_result['x_pos']="${position%%+*}"
	_result['y_pos']="${position#*+}"
	local mmSize=$(xrandr_query | sed -n "s/^${1}.* \(${num}\)mm x \(${num}\)mm$/\1x\2/p")
	local mm_width="${mmSize%%x*}"
	local mm_height="${mmSize#*x}"
	local x_dpi
	if [[ $mm_width -gt 0 ]]; then
		x_dpi=$(printf '%.2f\n' "$((10**3 * $width * 254 / $mm_width))e-4")
	fi
	local y_dpi
	if [[ $mm_height -gt 0 ]]; then
		y_dpi=$(printf '%.2f\n' "$((10**3 * $height * 254 / $mm_height))e-4")
	fi
	if [[ $mm_width == 0 ]]; then
		mm_width=
	fi
	if [[ $mm_height == 0 ]]; then
		mm_height=
	fi
	_result['mm_width']="${mm_width}"
	_result['mm_height']="${mm_height}"
	_result['x_dpi']="${x_dpi}"
	_result['y_dpi']="${y_dpi}"
}

print_connected_outputs() {
	xrandr_query >/dev/null # ensure global cache is populated
	local doPrintDetails='false'
	for opt in "$@"; do
		case $opt in
		--detail) shift; doPrintDetails='true';;
		*)
			printf 'Unknown option: %s\n\n' "$opt" >&2
			print_usage >&2
			return 1
		;;
		esac
	done
	get_connected_outputs
	local outputs=("${_result[@]}")
	{
		if [[ $doPrintDetails == 'true' ]]; then
			echo "OUTPUT;DEVICE;WIDTH;HEIGHT;X_POS;Y_POS;MM_WIDTH;MM_HEIGHT;X_DPI;Y_DPI"
		fi
		for output in "${outputs[@]}"; do
			printf "$output"
			if [[ $doPrintDetails == 'true' ]]; then
				get_details_for_output "$output"
				for i in device_name width height x_pos y_pos mm_width mm_height x_dpi y_dpi; do
					local value="${_result[$i]}"
					printf ';%s' "${value:-n/a}"
				done
			fi
			printf '\n'
		done
	} | column -s';' -t
}

is_output_config_valid() {
	local config="$1"
	if [[ -z $config ]]; then
		return 1
	fi
	local output="${config%%:*}"
	if [[ -z $output ]]; then
		return 1
	fi
	config="${config#$output}"; config="${config#:}"
	local modespec="${config%%:*}"
	if [[ -n $modespec && $modespec != '-' ]]; then
		local d='[[:digit:]]'
		if [[ ! $modespec =~ ^$d+x$d+(@$d+)?$ ]]; then
			return 1
		fi
	fi
	config="${config#$modespec}"; config="${config#:}"
	while [[ -n $config ]]; do
		local arg="${config%%:*}"
		if [[ -z $arg ]]; then
			return 1
		fi
		config="${config#$arg}"; config="${config#:}"
	done
	return 0
}

autosetup() {
	xrandr_query >/dev/null # ensure global cache is populated
	get_primary_output
	local primary="$_result"
	local output_configs=()
	get_connected_outputs
	for output in "${_result[@]}"; do
		local output_config="$output::"
		if [[ $output == $primary ]]; then
			output_config+='p'
		fi
		output_configs+=("$output_config")
	done
	setup_outputs "${output_configs[@]}"
}

split_mode_spec() {
	unset _result
	declare -Ag _result
	_result[width]="${1%%x*}"
	local rest_spec="${1#*x}"
	if [[ $rest_spec =~ @ ]]; then
		_result[height]="${rest_spec%%@*}"
		_result[rate]="${rest_spec#*@}"
	else
		_result[height]="$rest_spec"
		_result[rate]=''
	fi
}

get_mode_args_by_mode_spec() {
	split_mode_spec "$1"
	local width="${_result[width]}"
	local height="${_result[height]}"
	local rate="${_result[rate]}"
	unset _result
	declare -ag _result=($(cvt "$width" "$height" ${rate:+"$rate"} | tail -n1 | sed 's/^Modeline //'))
	_result[0]="${_result[0]:1:$(( ${#_result[0]} - 2 ))}" # remove quotes
}

does_mode_exist() {
	print_modes_for_output "$1" | grep "^$2$" &>/dev/null
}

get_mode() {
	# Usage: get_mode <OUTPUT_NAME> <MODE_SPEC>
	# creates the specified mode if it does not yet exist
	get_mode_args_by_mode_spec "$2"
	local mode_name="${_result[0]}"
	if ! does_mode_exist "$1" "$mode_name"; then
		echo "creating mode '$1: $mode_name'"
		xrandr --newmode "${_result[@]}"
		xrandr --addmode "$1" "$mode_name"
	fi
	_result="$mode_name"
}

parseAdditionalConfig() {
	unset _result
	declare -Ag _result=()
	local config="$1"
	while [[ -n "$config" ]]; do
		currentArg="${config%%:*}"
		config="${config#$currentArg}"; config="${config#:}"
		key="${currentArg%%=*}"
		val="${currentArg#$key}"; val="${val#=}"
		case $key in
		o) key='--orientation';;
		p) key='--primary';;
		r|refresh) key='--rate';;
		s) key='--size';;
		x) key='-x';;
		y) key='-y';;
		*) key="--$key";;
		esac
		_result["$key"]="$val"
	done
}

setup_output() {
	local output_name="${1%%:*}"
	local config="${1#*:}"
	local mode_spec="${config%%:*}"
	config="${config#*:}"
	local args=(--output "$output_name")
	if ! does_output_exist "$output_name"; then
		echo "Unknown output: $output_name"
		return 1
	fi
	if [[ $mode_spec == '-' ]]; then
		args+=(--off)
	else
		if [[ -z $mode_spec ]]; then
			get_highest_resolution "$output_name"
			args+=(--mode "$_result")
		elif does_mode_exist "$output_name" "$mode_spec"; then
			args+=(--mode "$mode_spec")
		else
			get_mode "$output_name" "$mode_spec"
			args+=(--mode "$_result")
		fi
		parseAdditionalConfig "$config"
		for opt in "${!_result[@]}"; do
			val="${_result["$opt"]}"
			args+=("$opt" ${val:+"$val"})
		done
	fi
	if [[ $DEBUG == 'true' ]]; then
		printf 'xrandr'; printf ' %s' "${args[@]}"; printf '\n'
	fi
	xrandr "${args[@]}"
}

setup_outputs() {
	xrandr_query >/dev/null # ensure global cache is populated
	for output_config in "$@"; do
		if [[ $output_config =~ ^-.* ]]; then
			echo "Expected display config but found option: $output_config"
			print_usage >&2
			return 1
		elif ! is_output_config_valid "$output_config"; then
			echo "Invalid display config: $output_config" >&2
			echo "See \`$app_name -h\` for correct format." >&2
			return 1
		fi
	done
	# all output configs are valid at this point
	for output_config in "$@"; do
		setup_output "$output_config"
	done
}

print_modes() {

	local outputs=()
	if [[ $# -eq 0 ]]; then
		get_connected_outputs
		outputs=("${_result[@]}")
	else
		for output in "$@"; do
			if does_output_exist "$output"; then
				outputs+=("$output")
			else
				echo "Unknown output: $output" >&2
				echo "See \`$app_name -l\` for valid names." >&2
				return 1
			fi
		done
	fi
	if [[ $# -eq 1 && ${#outputs[@]} -eq 1 ]]; then
		print_modes_for_output "${outputs[0]}"
	else
		for output in "${outputs[@]}"; do
			echo "$output:"
			print_modes_for_output "$output" | sed 's/^/\t/'
		done
		
	fi
}

print_license_info() {
	# extract license info from THIS file
	sed -n '5,18p' "$0" | sed -E 's/# ?//'
}

print_version() {
	cat <<- EOF
		$app_name, version $app_version

		$(print_license_info)

	EOF
}

if ! (return 0 &>/dev/null); then
	# script is executed rather than sourced
	if [[ $# == 0 || -z $1 ]]; then
		print_usage >&2
		exit 1
	fi
	case $1 in
	-a|--autosetup) autosetup;;
	-h|--help) print_help;;
	-l|--list-displays) 
		shift
		print_connected_outputs "$@"
	;;
	-L)
		shift
		print_connected_outputs --detail "$@"
	;;
	-m|--list-modes)
		shift
		print_modes "$@"
	;;
	-V|--version) print_version;;
	-*) print_usage >&2; exit 1;;
	*) setup_outputs "$@";;
	esac
fi

