# display-setup

A wrapper around `xrandr` for easier setting-up displays.

## Usage

See: `display-setup -h`

### Examples

```
display-setup -a
```

Automatically setup all connected displays to their highest possible screen resolution.
The screen with the largest possible screen resolution will be set as primary screen.

```
display-setup HDMI-1::p
```
  
Setup display `HDMI-1` to be the primary display with its highest possible resolution.

```
display-setup HDMI-1:1920x1080
```

Setup display `HDMI-1` for a screen resolution of 1920x1080 px.

```
display-setup HDMI-1:1920x1080@60
```

Setup display `HDMI-1` for a screen resolution of 1920x1080 px at 60 Hz.


```
display-setup HDMI-1:-
```

Disable display `HDMI-1`.


```
display-setup HDMI-1::p DP-1::right-of=HDMI-1
```

Setup display `HDMI-1` as the primary display with its highest possible resolution
and `DP-1` as secondary display with its highest possible resolution. Positions display
`DP-1` to the right of display `HDMI-1`.
(see the `--right-of` option in the xrandr manpage)

## License

©2020 Andanan

Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at  


> http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either  
express or implied. See the License for the specific language  
governing permissions and limitations under the License.  

