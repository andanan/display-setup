# Copyright 2020 Andanan
#
# File: display-setup.bash
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq (${DESTDIR}${PREFIX},)
	PREFIX = ${HOME}/.local
	ifeq (0,$(shell id -u))
		PREFIX = /usr/local
	endif
endif
app_name = display-setup
exe_name = ${app_name}
app_dir = ${DESTDIR}${PREFIX}/share/${app_name}
bin_dir = ${DESTDIR}${PREFIX}/bin

install:
	install -d ${app_dir}
	install -m 644 Makefile README.md ${app_dir}
	install -m 755 ${exe_name}.bash ${app_dir}
	install -d ${bin_dir}
	ln -sf ${app_dir}/${exe_name}.bash ${bin_dir}/${exe_name}
	echo "DESTDIR=${DESTDIR}" >> ${app_dir}/install-vars.info
	echo "PREFIX=${PREFIX}" >> ${app_dir}/install-vars.info
	echo "app_name=${app_name}" >> ${app_dir}/install-vars.info
	echo "app_dir=${app_dir}" >> ${app_dir}/install-vars.info
	echo "bin_dir=${bin_dir}" >> ${app_dir}/install-vars.info

uninstall:
	rm ${bin_dir}/${exe_name}
	rm ${app_dir}/*
	rmdir -p ${bin_dir} ${app_dir} >/dev/null 2>&1 || true

update:
	if [ -d .git ]; then git pull; else echo "Not installed via git!" >&2; fi

